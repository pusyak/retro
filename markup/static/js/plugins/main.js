$(document).ready(function() {

  var owl = $(".main_slider");
 
  owl.owlCarousel({
    singleItem : true,
    autoPlay: 3000,
    stopOnHover: true,
    mouseDrag: false,
    transitionStyle : "fade"
  });

  objectFit.polyfill({
    selector: 'img', // this can be any CSS selector
    fittype: 'cover', // either contain, cover, fill or none
    disableCrossDomain: 'true' // either 'true' or 'false' to not parse external CSS files.
  });
 
});

